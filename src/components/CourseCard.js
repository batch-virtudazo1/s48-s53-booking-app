//import {useState} from 'react';
import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function CourseCard({courseProp}) {
	//console.log(props);
	//console.log(typeof props);

	const { name, description, price, _id} = courseProp;
	//console.log(props);

	/*const [count, setCount] = useState(0);
	const [seatCount, setSeatCount] = useState(10);
	console.log(useState(0));
	console.log(useState(10));

	function enroll(){
		
		console.log(seatCount);
		if(seatCount !== 0){
			setCount(count + 1);
			setSeatCount(seatCount - 1);
		} else {
			alert("No more seats available, check back later.");
		}
		//console.log(`Enrollees: ${count}`);
	}*/
	return (
			<Card  className = "courseCard">
			     <Card.Body>
			        <Card.Title>{name}</Card.Title>
			      	<Card.Subtitle>Description:</Card.Subtitle>
			        <Card.Text>
						{description}
			        </Card.Text>
			        <Card.Subtitle>Price:</Card.Subtitle>
			        <Card.Text>{price}</Card.Text>
			        <Link className = "btn btn-primary" to = {`/courseView/${_id}`}>View Details</Link>
			    </Card.Body>
			    </Card>
		)
}