import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "Peter Parker";
// // JSXML(<h1>) etc
// const element = <h1> Hello, {name}</h1>

// const user = {
//   firstName: 'Levi',
//   lastName: "Akerman"
// };

// const formatName = (user) => {
//   return user.firstName +' '+ user.lastName;
// }
// const fullName = <h1> Hello, {formatName(user)}</h1>

// // tp display the content
// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(fullName);