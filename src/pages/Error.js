import {Button, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function Error() {

	const head = "";
	return (
		<>
		<h1></h1>
		<p>The page you are looking for is not found.</p>
		<Button variant = 'primary' as = {Link} to = '/'>Back to Home</Button>
		</>
		)
}